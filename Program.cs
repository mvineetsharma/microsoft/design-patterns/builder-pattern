﻿using System;

// Define the Product class (Fruit in this case)
class Fruit
{
    public string Name { get; set; }
    public string Color { get; set; }
    public double Weight { get; set; }

    public void Display()
    {
        Console.WriteLine($"Fruit: {Name}, Color: {Color}, Weight: {Weight} grams");
    }
}

// Define the FruitBuilder interface
interface IFruitBuilder
{
    void SetName(string name);
    void SetColor(string color);
    void SetWeight(double weight);
    Fruit Build();
}

// Implement the FruitBuilder
class ConcreteFruitBuilder : IFruitBuilder
{
    private Fruit fruit = new Fruit();

    public void SetName(string name)
    {
        fruit.Name = name;
    }

    public void SetColor(string color)
    {
        fruit.Color = color;
    }

    public void SetWeight(double weight)
    {
        fruit.Weight = weight;
    }

    public Fruit Build()
    {
        return fruit;
    }
}

// Define the Director class (optional)
class FruitDirector
{
    private IFruitBuilder builder;

    public FruitDirector(IFruitBuilder builder)
    {
        this.builder = builder;
    }

    public void ConstructBanana()
    {
        builder.SetName("Banana");
        builder.SetColor("Yellow");
        builder.SetWeight(120);
    }

    public void ConstructApple()
    {
        builder.SetName("Apple");
        builder.SetColor("Red");
        builder.SetWeight(150);
    }
}

class Program
{
    static void Main()
    {
        // Create a builder
        IFruitBuilder builder = new ConcreteFruitBuilder();

        // Create a director (optional)
        FruitDirector director = new FruitDirector(builder);

        // Construct a Banana
        director.ConstructBanana();
        Fruit banana = builder.Build();
        banana.Display();

        // Construct an Apple
        director.ConstructApple();
        Fruit apple = builder.Build();
        apple.Display();
    }
}
